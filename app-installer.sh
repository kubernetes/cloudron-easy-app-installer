#!/bin/bash

# Set HOME-Folder to /app/data because /root is read-only
export HOME=/app/data

setup() {
  # Install cloudron cli with npm and use /app/data/cache for caching because normal cache path is not writable
  npm install --cache /app/data/cache/ cloudron --prefix /app/data/

  # Create a new config.yml
  if [ ! -f "config.yml" ]; then
    cat << EOF > config.yml
ACCESS_TOKEN=your_access_token (from CL dash>profile or existing R/W)
CLOUDRON_SERVER=my.example.com
CUSTOM_APP_IMAGE=custom_app_image
LOCATION_OF_INSTALLED_APP=subdomain or location where the App should be installed to
EOF
    echo "config.yml has been created. Please set the values according to your environment!"
  fi
}

load_config() {
  # Read config.yml
  while IFS='=' read -r key value; do
    if [[ $key && $value ]]; then
      export "$key"="$value"
    fi
  done < config.yml

  # write config.yml variables to screen
  echo "Access Token: $ACCESS_TOKEN"
  echo "Cloudron Server: $CLOUDRON_SERVER"
  echo "Custom App Image: $CUSTOM_APP_IMAGE"
  echo "Location of Installed App: $LOCATION_OF_INSTALLED_APP"

  # install custom app to cloudron
  /app/data/node_modules/cloudron/bin/cloudron --token "$ACCESS_TOKEN" --server "$CLOUDRON_SERVER" install --image "$CUSTOM_APP_IMAGE" --no-wait -l "$LOCATION_OF_INSTALLED_APP"
}

# Check if config.yml needs to be created or if we can continue with using the config.yml
if [ -f "config.yml" ]; then
  load_config
else
  setup
fi
