# THIS PACKAGE IS OUTDATED - PLEASE CHECK THE EASY INSTALLER APPROACH IN THE CLOUDRON TEMPLATE PACKAGE: https://git.cloudron.io/kubernetes/cloudron-package-template

# cloudron-easy-app-installer

This is an attempt to enable Cloudron Admins to install custom Cloudron Apps to Cloudron Instances without the need to build the Docker images themselves or host these images on their own Docker Registry.

**THIS IS STILL WORKING IN PROGRESS, PLEASE USE WITH CARE AND EXPECT THAT THINGS DOES NOT WORK AS EXPECTED!**

This script can be discussed in the [Cloudron Forum](https://forum.cloudron.io/topic/10966/installing-custom-apps-on-cloudron?_=1706429907432)

# HOW-TO use this app installer for custom Cloudron Apps

* Create a surfer App instance in your Cloudron instance
* Create a sub-folder with File Manager in the surfer app instance
* Copy this script (app-installer.sh) to the new created sub-folder of the surfer instance (via File Manager). **Take attention to NOT copy your files to the /app/data/public folder as this could leak your API Token!**
* You may need to run `dos2unix app-installer.sh`
* You should check if you copy & paste the script, that no line breaks are breaking the code (have a look at long lines!)
* Copy the CloudronManifest.json and favicon.png to the new created sub-folder
* Go to terminal and execute the command: `chmod +x app-installer.sh`
* Execute `./app-installer.sh`
* It should create a config.yml file
* Provide your details to the config.yml
* Execute `./app-installer.sh` again to deploy the custom app according to the configuration in config.yml
